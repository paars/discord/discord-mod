"use strict"

const path = require("path")
const fs = require("fs-extra")

exports.update = dir => {
    fs.copySync(__dirname, path.join(dir, "resources", "app"))
}