"use strict"

const fs = require("fs")
const path = require("path")

exports.initialize = mw => {
    mw.webContents.on("dom-ready", ev => {
            mw.webContents.executeJavaScript(
                `window._injectDir = "${path.dirname(require.resolve("discord-injections")).replace(/\\/g, '/')}"` +  
                fs.readFileSync(require.resolve("discord-injections/DomReady/inject.js"), 'utf8')
            )
    })
}