"use strict"

const electron = require("electron")
const util = require("util")
const fs = require("fs")
const path = require("path")
const Module = require("module")

// fix rooot patch calls
const root = path.join(__dirname, "..", "app.asar")
electron.app.getAppPath = () => root

const oldLoader = Module._extensions['.js']
let monkeyPatcher = 0

console.log("[Injector] adding custom .js compiler")

Module._extensions['.js'] =  (module, filename) => {
    let content = fs.readFileSync(filename, 'utf8');

    if (filename == path.join(root, "index.js")) {
        console.log("[Injector] patching index.js")
        content = content.replace("global.mainWindowId = mainWindow.id;", `
        global.mainWindowId = mainWindow.id;
        require('${path.join(__dirname, "main-window").replace(/\\/g, "\\\\")}').initialize(mainWindow);
        `).replace("webPreferences: {", `
        transparency: true,
        transparent: true,
        backgroundColor: '#00000000',
        webPreferences: { preload: '${require.resolve("discord-injections/Preload/index.js").replace(/\\/g, "/")}',
        `)
        monkeyPatcher++
    }

    if (filename == path.join(root, "SquirrelUpdate.js")) {
        console.log("[Injector] patching SquirrelUpdate.js")
        content = content.replace("app.once('will-quit', function () {", `
        app.once('will-quit', function () {
            require('${path.join(__dirname, "squirrel-update").replace(/\\/g, "\\\\")}').update(_path2.default.resolve(rootFolder, 'app-' + newVersion));
        `)
        monkeyPatcher++
    }

    if (monkeyPatcher >= 2) {
        console.log("[Injector] all files patched, revert to original loader")
        Module._extensions['.js'] = oldLoader
    }

    return module._compile(content, filename);
}

// rewrite root module
const pkg = require(path.join(root, "package.json"))
console.log("[Injector] loading discord!")
Module._load(path.join(root, pkg.main), null, true)